import React from 'react';
import SearchBar from "material-ui-search-bar";
import ListLIA from "../components/ListLIA";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
    root: {
        background: "#eeedec",
        padding: "0",
        margin: "0"
    },
    searchBar: {
        width:"80%",
        margin:"20px",
        marginTop: "20px"
    },
    quickButton:{
        background: "#efd9c1"
    }

});

export default function SearchLia() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Button className={classes.quickButton} size="small" variant="contained">
                Sparade
            </Button>
            <Button className={classes.quickButton} size="small" variant="contained">
                Ansökta
            </Button>
            <SearchBar className={classes.searchBar}/>
            <ListLIA/>
        </div>
    );
}
