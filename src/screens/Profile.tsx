import React, {useRef, useLayoutEffect, useState, useEffect, Component} from 'react';
import ReactDOM from 'react-dom';
import clsx from 'clsx';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import {ThemeProvider} from '@material-ui/styles';
import Navbar from "../components/Navbar";
import SideNavElements from "../components/SideNavElements"
import {MarginItemsLeft} from "../components/SideNavElements";
import {addN01} from "../components/SideNavElements";
import SearchLia from "./SeachLia";
import LogoPlaceholder from "../components/LogoPlaceholder";
import {useMediaQuery} from 'react-responsive';
import CompanyProfile from "./CompanyProfile";
import StudentMainScreen from "./StudentMainScreen";
import Routs from "../root/Routs";
import {HashRouter as Router, Route, Switch} from 'react-router-dom';

const addNew = 0
const useStyles = makeStyles((theme) => ({
    appBar: {marginLeft: addNew}
}))


const ApplyResp = () => {
    const isDesktopOrLaptop = useMediaQuery({
        query: '(min-width: 1224px)'
    })
    const isBigScreen = useMediaQuery({query: '(min-width: 1824px)'})
    const isTabletOrMobile = useMediaQuery({query: '(max-width: 1224px)'})
    const isPortrait = useMediaQuery({query: '(orientation: portrait)'})
    const isRetina = useMediaQuery({query: '(min-resolution: 2dppx)'})

    return <div>
        <h1>Device Test!</h1>
        {isDesktopOrLaptop && <p>You are a desktop or laptop</p>}
        {isBigScreen && <p>You have a huge screen</p>}
        {isTabletOrMobile && <p>You are a tablet or mobile phone</p>}
        <p>Your are in {isPortrait ? 'portrait' : 'landscape'} orientation</p>
        {isRetina && <p>You are retina</p>}
    </div>
}

function Profile() {
    const isBigScreen = useMediaQuery({query: '(min-width: 1824px)'})
    const isDesktopOrLaptop = useMediaQuery({query: '(min-width: 1224px)'})
    const isTabletOrMobile = useMediaQuery({query: '(max-width: 1224px)'})
    const classes = useStyles();
    const targetRef = React.createRef<HTMLDivElement>()
    const [dimensions, setDimensions] = useState({width: 0, height: 0});
    const [data, setData] = useState(73);

    useLayoutEffect(() => {
        if (targetRef.current) {
            const boundingRect = targetRef.current.getBoundingClientRect()
            const {width, height} = boundingRect
            setDimensions({width: Math.round(width), height: Math.round(height)})
            setData(width);
        }
    }, []);


//Style for divs containing components style={{marginLeft: dimensions.width}}
    return (
        <div className="Profile" style={{background: '#eeedec'}}>
            <header className="Profile-header">
                <div style={{height: '12vh'}}>
                    <LogoPlaceholder/>
                </div>
            </header>
            {isDesktopOrLaptop &&
            <div id='componentsContainer'>
                <div style={{paddingLeft: 217, position: 'fixed', width: '100vw', zIndex: 1,}}>
                    <Navbar/>
                </div>
                <div id='r00' style={{display: 'inline-block'}} ref={targetRef}>
                    <SideNavElements/>
                </div>
                <div style={{marginLeft: 217, marginTop: '64px'}}>
                    <Routs/>
                </div>
            </div>
            }
            {isTabletOrMobile &&
            <div id='componentsContainer'>
                <div style={{paddingLeft: 70, position: 'fixed', width: '100vw', zIndex: 1,}}>
                    <Navbar/>
                </div>
                <div id='r00' style={{display: 'inline-block'}} ref={targetRef}>
                    <SideNavElements/>
                </div>
                <div style={{marginTop: '64px'}}>
                    <Routs/>
                </div>
            </div>
            }
        </div>
    );
}

/* <div style={{display: 'none'}}> */

export default Profile;