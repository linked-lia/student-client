import React from 'react';
import {makeStyles, createStyles, Theme} from '@material-ui/core/styles';
import ButtonMenuCompProfile from "../components/ButtonMenuCompProfile";
import LogoImage from "../components/LogoImage";
import Gallery from "../components/Gallery";
import BigImage from "../components/BigImage";
import TextArea from "../components/TextArea";
import {Grid} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
           //stylings :)
        }
    }),
);

export default function CompanyProfile() {
    const classes = useStyles();

    return (
        <Grid container className={classes.root}>
            <BigImage/>
            <LogoImage/>
            <ButtonMenuCompProfile/>
            <TextArea/>
            <Gallery/>
        </Grid>
    );
}
