import React from 'react';
import SearchBar from "material-ui-search-bar";
import ListSavedLIA from "../components/ListSavedLIA";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {Link, HashRouter as Router} from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        background: "#eeedec",
        padding: "0",
        margin: "0"
    },
    searchBar: {
        width:"80%",
        margin:"20px",
        marginTop: "20px",
    },
    quickButton:{
        background: "#efd9c1",
        marginLeft: "20px"
    }

});

export default function SavedLia() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <SearchBar className={classes.searchBar}/>
            <Button className={classes.quickButton} size="small" variant="contained">
                Filtrera
            </Button>
            <Button className={classes.quickButton} size="small" variant="contained">
                Sortera
            </Button>
            <ListSavedLIA/>
        </div>
    );
}
