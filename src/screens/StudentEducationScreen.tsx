import React, {ChangeEvent, FormEvent, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField, Card, Typography} from "@material-ui/core";
import IconButton from "@mui/material/IconButton";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import EditRoundedIcon from '@mui/icons-material/EditRounded';
import SimpleCard from "../components/SimpleCard";
import PopUpWindow from "../components/PopUpWindow";
import StudentWorkForm from "../components/StudentWorkForm";
import StudentEducationForm from "../components/StudentEducationForm";
const useStyles = makeStyles((theme) => ({
    root:{

        margin:"20px",
        padding: "10px",
        backgroundColor:"#efd9c1"

    }
}));

export default function StudentEducationScreen() {
    const classes = useStyles();
    const [openWindow, setOpenWindow] = React.useState(false);
    const closeWindow = () => {
        setOpenWindow(false);
    }


    return (
        <Card className={classes.root}>
            <div>
                <IconButton  onClick={(e)=>setOpenWindow(true)} size="large" style={{backgroundColor:"#ddb6a2", marginRight:"5px",borderRadius:"50%",
                    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"}}>
                    <AddRoundedIcon style={{color:"#555d5c"}}/>
                </IconButton>
            </div>
            <SimpleCard company="Stockholm University" position="MSC in Computer Science" from="2007" to="2008"/>
            <PopUpWindow title="Education" openWindow={openWindow} closeWindow={closeWindow}>
                <StudentEducationForm />

            </PopUpWindow>

        </Card>
    )
}