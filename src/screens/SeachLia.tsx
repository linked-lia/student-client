import React from 'react';
import SearchBar from "material-ui-search-bar";
import ListLIA from "../components/ListLIA";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {Link, HashRouter as Router} from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        background: "#eeedec",
        padding: "0",
        margin: "0"
    },
    searchBar: {
        width: "80%",
        margin: "20px",
        marginTop: "20px"
    },
    quickButton: {
        color: "#726e6e",
        background: "#efd9c1"
    },
    quickButtonText: {
        textDecoration: 'none'
    }
});

export default function SearchLia() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Link to='savedLia' className={classes.quickButtonText}>
                <Button className={classes.quickButton} size="small" variant="contained">
                    Sparade
                </Button>
            </Link>
            <Link to='appliedLia' className={classes.quickButtonText}>
                <Button className={classes.quickButton} size="small" variant="contained">
                    Ansökta
                </Button>
            </Link>
            <SearchBar className={classes.searchBar}/>
            <ListLIA/>
        </div>
    );
}
