import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ButtonMenuLiaAdd from "../components/ButtonMenuLiaAdd";
import LogoImage from "../components/LogoImage";
import LiaAddTextArea from "../components/LiaAddTextArea";
import {Grid} from "@material-ui/core";
import LiaAddImage from "../components/LiaAddImage";

const useStyles = makeStyles({
    root: {
        background: "#eeedec"
    }
});

export default function LiaAddView() {
    const classes = useStyles();

    return (
        <Grid container className={classes.root}>
            <LiaAddImage/>
            <LogoImage/>
            <ButtonMenuLiaAdd/>
            <LiaAddTextArea/>
        </Grid>
    );
}
