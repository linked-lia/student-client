import React, {ChangeEvent, FormEvent, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Card, Paper, Tab, Tabs, TextField} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import StudentApi from "../api/StudentApi";
import Image from "../assets/media/profilepic.png";
import StudentAddFileForm from "../components/StudentAddFileForm";
import StudentInfoForm from "../components/StudentInfoForm";
import StudentWorkScreen from "./StudentWorkScreen";
import StudentEducationScreen from "./StudentEducationScreen";
import StudentSkillScreen from "./StudentSkillScreen";
import {Typography} from "@mui/material";
import PopUpWindow from "../components/PopUpWindow";
import StudentResumeScreen from "./StudentResumeScreen";




const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            //position: "relative",
            minHeight:"30vh",
            margin:"20px",
            background:"#a9b7c0",
            textAlign:"center",
            display: "flex"
        },
        try:{
            width: "70%",
            textAlign: "center"

        },
        studenttab:{
            margin: "20px",
            marginBottom:"0px",
            background:"#efd9c1",
            justifyContent:"space-between",
        },
        selected:{
            margin: "20px",
            marginTop:"0px",
            background:"#EFD9c1",

        },
     round: {
         /*position: "absolute",
         top:"50px",
         left:"50px",*/
         margin: "10px",
         marginLeft:"30px",
         height: "180px",
         width: "150px",
         background: "white",
         borderRadius: "50%",
         border: "5px",
         display: "inline-block",
         //backgroundImage: `url(${Image})`
},
        studentname:{

            margin:"20px"
            //position: "inherit",
/*
            alignItems:"center",
            textAlign:"center"*/
        }



    }),
);
export default function StudentMainScreen() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [openWindow, setOpenWindow] = React.useState(false);
    const handleChange = (event: any, newValue: number) => {
        setValue(newValue);
    };

    const closeWindow = () => {
        setOpenWindow(false);
    }
    return(
        <div className={classes.try}>
            <Paper elevation={20} className={classes.root} >
<div>
                    <img src={Image} style={{height:"200px",width:"150px" }} className={classes.round}/>
                    <Typography  onClick={()=>setOpenWindow(true)}>
                        <Button variant="text">Change Profile Picture</Button>
                    </Typography>
</div>
                <div className={classes.studentname}>
                    <h2>Shakir Ahmed Zahedi</h2>
                </div>

            </Paper>
           <Paper elevation={20} className={classes.studenttab} >
                <Tabs value={value} onChange={handleChange} style={{backgroundColor:"#cccbc6" }} >
                    <Tab label="Info"  />
                    <Tab label="Education"  />
                    <Tab label="Experience"  />
                    <Tab label="Skills"  />
                    <Tab label="Resume"  />
                </Tabs>
                {value===0 && <StudentInfoForm/>}
                {value===1 && <StudentEducationScreen/>}
                {value===2 && <StudentWorkScreen/>}
                {value===3 && <StudentSkillScreen/>}
               {value===4 && <StudentResumeScreen/>}
            </Paper>


            <PopUpWindow openWindow={openWindow} closeWindow={closeWindow}>
               <StudentAddFileForm/>
            </PopUpWindow>


        </div>
    )
}