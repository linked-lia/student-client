import React from 'react';
import SearchBar from "material-ui-search-bar";
import ListCompany from "../components/ListCompany";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {Link, HashRouter as Router} from 'react-router-dom';
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import {Autocomplete, Box, TextField} from "@mui/material";

const useStyles = makeStyles({
    root: {
        background: "#eeedec",
        padding: "0",
        margin: "0"
    },
    searchBar: {
        width: "80%",
        margin: "20px",
        marginTop: "20px"
    },
    quickButton: {
        color: "#726e6e",
        background: "#efd9c1"
    },
    quickButtonText: {
        textDecoration: 'none'
    }
});

const cities = [
    {label: 'Arninge'},
    {
        label: 'Stockholm'

    },
    {
        label: 'Malmo'

    },
    {
        label: 'Lund'
    },
    {
        label: 'Växjö'

    },
    {
        label: 'Uppsala'
    },
    {
        label: 'Gotland'

    },
];


export default function SearchCompany() {
    const classes = useStyles();

    return (
        <div className={classes.root}>

            {/*<Link to='savedLia' className={classes.quickButtonText}>
                <Button className={classes.quickButton} size="small" variant="contained">
                    Favourite / Book marked
                </Button>
            </Link>
            <Link to='appliedLia' className={classes.quickButtonText}>
                <Button className={classes.quickButton} size="small" variant="contained">
                    History
                </Button>
            </Link>*/}

            {/*<Card className={classes.root}>*/}

            <Autocomplete className={classes.searchBar}
                          id="country-select-demo"
                          sx={{width: 300}}
                          options={cities}
                          autoHighlight
                          getOptionLabel={(option) => option.label}
                          renderOption={(props, option) => (
                              <Box component="li" sx={{'& > img': {mr: 2, flexShrink: 0}}} {...props}>
                                  <img
                                      loading="lazy"
                                      width="20"

                                      alt=""
                                  />
                                  {option.label}
                              </Box>
                          )}

                          renderInput={(params) => (
                              <TextField
                                  {...params}
                                  label="Choose a city"
                                  inputProps={{
                                      ...params.inputProps,
                                      autoComplete: 'new-password', // disable autocomplete and autofill
                                  }}
                              />
                          )}
                                      />


            {/*</Card>*/}
            <SearchBar className={classes.searchBar}/>
            <ListCompany/>
        </div>
    );
}
