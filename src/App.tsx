import React from 'react';
import './App.css';
import Profile from "./screens/Profile";
import {HashRouter as Router} from 'react-router-dom';


function App() {
    return (
        <Router>
            <Profile/>
        </Router>
    );
}

export default App;
