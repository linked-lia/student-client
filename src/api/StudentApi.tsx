import React, {useState, useEffect} from 'react'


class StudentApi {
    getAll = async () => {
        const response = await fetch('http://localhost:8088/students');
        const result = await response.json();
        return result;
    }
    addStudent = async (data = {}) => {
        const response = await fetch('http://localhost:8088/students/add',
            {
                method: 'POST',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(data)
            });
        const result = await response.json();
        return result;
    }
    addStudentPic = async (id:string, data:any) => {
        const response = await fetch('http://localhost:8088/students/' + id,
            {
                method: 'POST',
                //headers: {"Content-type": "multipart/form-data"},
                body: data//JSON.stringify(data)
            });
        const result = await response.json();
        return result;
    }




    return:any;
}

export default new StudentApi ();
