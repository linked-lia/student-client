import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Navbar from "./Navbar";
import SideNavElements from "./SideNavElements";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
    }),
);

export default function GridTest() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3} justifyContent="flex-end">
                <Grid item xs={4} sm={4}>
                    <SideNavElements/>
                </Grid>
                <Grid item xs={12} sm={3}>
                    <Navbar/>
                </Grid>
            </Grid>
        </div>
    );
}
