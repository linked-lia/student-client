import React, {ChangeEvent, FormEvent, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import { Paper, TextField} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import StudentApi from "../api/StudentApi";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        form:{
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch',
            },}


        }),
);
type FormData = {
    firstName: string;
    lastName: string;
    password:string;
    email:string;
};
export default function CreateNewStudentForm(){
    const classes = useStyles();
    const { register, setValue, handleSubmit, formState: { errors } } = useForm<FormData>();

    const [firstName,setFirstName]=useState("");
    const [lastName,setLastName]=useState("");
    const [password,setPassword]=useState("");
    const [email,setEmail]=useState("");
    const handleFirstNameChange=(event: ChangeEvent<HTMLInputElement>)=>{
        let newName = event.target.value;
       //ameValidation(newName);
        setFirstName(newName);
    }
    const handleLastNameChange=(event: ChangeEvent<HTMLInputElement>)=>{
        let newName = event.target.value;
        //ameValidation(newName);
        console.log(newName)
        setLastName(newName);
    }
    const handlePasswordChange=(event: ChangeEvent<HTMLInputElement>)=>{
        let pass = event.target.value;
        //ameValidation(newName);
        setPassword(pass);
    }
    const handleEmailChange=(event: ChangeEvent<HTMLInputElement>)=>{
        let newemail = event.target.value;
        //ameValidation(newName);
        console.log(newemail)
        setEmail(newemail);

    }
    const onSubmit = handleSubmit((data) =>{
        StudentApi.addStudent({firstName:data.firstName,lastName:data.lastName, password:data.password,email:data.email})
            .then(res=> console.log(res))
            .catch(err=> alert(err))
    } );


    return(
        <>
            <Paper elevation={20} className={classes.root} >
                <form className={classes.form} noValidate autoComplete="off"  onSubmit={onSubmit}>
                    <TextField {...register("firstName")}
                        id="outlined-name-input"
                        label="FirstName"
                        value={firstName}
                        variant="outlined"
                        style={{width:"115%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                        required
                        onChange={handleFirstNameChange}

                    />
                    <TextField {...register("lastName")}
                        id="outlined-lastname-input"
                        value={lastName}
                        variant="outlined"
                        style={{width:"115%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                        required
                        onChange={handleLastNameChange}

                    />
                    <TextField {...register("password")}
                        id="outlined-password-input"
                        label="Password"
                        value={password}
                        variant="outlined"
                        style={{width:"85%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                        required
                        onChange={handlePasswordChange}

                    />
                    <TextField {...register("email")}
                        id="outlined-email-input"
                        label="E-mail"
                        value={email}
                        variant="outlined"
                        style={{width:"115%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                        required
                        onChange={handleEmailChange}

                    />
                    <Button type="submit"  color="primary"  >
                        Save

                    </Button>


                </form>

            </Paper>
        </>
    )


}