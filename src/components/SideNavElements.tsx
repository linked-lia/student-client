import React, {useRef, useLayoutEffect, useState, useEffect, Component} from 'react';
import clsx from 'clsx';
import {makeStyles, useTheme} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Box from '@mui/material/Box';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import LibraryBooksOutlined from '@material-ui/icons/LibraryBooksOutlined';
import LocationCityOutlined from '@material-ui/icons/LocationCityOutlined';
import PermMediaOutlined from '@material-ui/icons/PermMediaOutlined';
import HistoryOutlined from '@material-ui/icons/HistoryOutlined';
import SupervisorAccountOutlined from '@material-ui/icons/SupervisorAccountOutlined';
import IndeterminateCheckBoxOutlined from '@material-ui/icons/IndeterminateCheckBoxOutlined';
import EmojiEmotionsOutlined from '@material-ui/icons/EmojiEmotionsOutlined';
import WbIncandescentOutlined from '@material-ui/icons/WbIncandescentOutlined';
import ViewModuleOutlined from '@material-ui/icons/ViewModuleOutlined';
import BlurOnOutlined from '@material-ui/icons/BlurOnOutlined';
import {useMediaQuery} from 'react-responsive';
import Button from '@mui/material/Button';
import {withRouter} from 'react-router-dom';
import {Link, HashRouter as Router} from 'react-router-dom';


const drawerWidth = 217;

export const useStyles = makeStyles((theme) => ({
    root: {
        display: 'inline-block',
        '@global': {
            '::-webkit-scrollbar': {
                display: 'none'
            },
            '-ms-overflow-style': 'none',
        },
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        width: 70,
        left: 0,
        boxShadow: 'none'
    },
    appBarShift: {
        width: `70px`,
    },
    displayNone: {
        display: 'none',
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        top: '12vh',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },


    drawerClose: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        //width: theme.spacing(7) + 1,
        width: 0,
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9) + 1,
        },
        '@media (min-width: 600px)': {
            width: 0
        }
    },


    paper: {
        top: '12vh',
        background: '#C7D8C6',
        color: '#ffffff',
    },
    content: {
        top: '100px',
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    listItemText: {
        color: '#ffff',
        textDecoration: 'none'
    }
}));


export default function SideNavElements() {
    let setMarginLeft = 73;
    const [isActive, setActive] = useState("false");
    const classes = useStyles();
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    const targetRef = useRef<HTMLDivElement>()
    const [dimensions, setDimensions] = useState({width: 0, height: 0});
    const [data, setData] = useState(73);
    const isBigScreen = useMediaQuery({query: '(min-width: 1824px)'})
    const isDesktopOrLaptop = useMediaQuery({query: '(min-width: 1224px)'})
    const isTabletOrMobile = useMediaQuery({query: '(max-width: 1224px)'})


    //Responsive
    const [state, setState] = React.useState({
        left: false,
    });

    const toggleDrawer = (anchor: any, open: any) => (event: any) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        setState({...state, [anchor]: open});
    };


    const handleDrawerOpen = () => {
        setOpen(true);
        setMarginLeft = 240
        setData(setMarginLeft)
        return ({setMarginLeft})

    };

    const handleDrawerClose = () => {
        setOpen(false);
        setMarginLeft = dimensions.width
        setData(setMarginLeft)
        return ({setMarginLeft})
    };
    useLayoutEffect(() => {
        if (targetRef.current) {
            setDimensions({
                width: targetRef.current.offsetWidth,
                height: targetRef.current.offsetHeight
            });
        }
    }, []);

    const links = [
        {
            'name': 'LIA platser',
            'link': '/searchLia'
        },
        {
            'name': 'Hitta företag',
            'link': '/searchCompany'
        },
        {
            'name': 'Sparade LIA',
            'link': '/savedLia'
        },
        {
            'name': 'Ansökta LIA',
            'link': '/appliedLia'
        },
        {
            'name': 'Matchningar',
            'link': '/matches'
        },
        {
            'name': 'Tips & Tricks',
            'link': '/tipsAndTricks'
        },
        {
            'name': 'Redigera layout +',
            'link': '/editLayout'
        }
    ];

    const additionalLinks = [
        {
            'name': 'Kontakta Admin',
            'link': '/contactAdmin'
        },
        {
            'name': 'Avsluta Konto',
            'link': '/deleteAccount'
        },
        {
            'name': 'Logga Ut',
            'link': '/welcomePage'
        }
    ];

    // {['LIA platser', 'Hitta företag', 'Sparade LIA', 'Sökta LIA', 'Kontakta Admin', 'Avsluta konto', 'Matchningar', 'Tips & tricks' , 'Redigera layout +'].map((text, index) => (


    const menuItems = (
        <div>
            <Divider/>
            <List>
                {links.map((links, index) => (
                    <Link to={links.link} style={{textDecoration: 'none'}}>
                        <ListItem button key={links.name}>
                            <ListItemIcon>
                                {index === 0 && <LibraryBooksOutlined/>}
                                {index === 1 && <LocationCityOutlined/>}
                                {index === 2 && <PermMediaOutlined/>}
                                {index === 3 && <HistoryOutlined/>}
                                {index === 4 && <SupervisorAccountOutlined/>}
                                {index === 5 && <IndeterminateCheckBoxOutlined/>}
                                {index === 6 && <EmojiEmotionsOutlined/>}
                                {index === 7 && <WbIncandescentOutlined/>}
                                {index === 8 && <ViewModuleOutlined/>}
                            </ListItemIcon>
                            <ListItemText className={classes.listItemText}>
                                {links.name}
                            </ListItemText>
                        </ListItem>
                    </Link>
                ))}
            </List>
            <Divider/>
            <List>
                {additionalLinks.map((additionalLinks, index) => (
                    <Link to={additionalLinks.link} style={{textDecoration: 'none'}}>
                        <ListItem button key={additionalLinks.name}>
                            <ListItemIcon>
                                {index === 0 && <BlurOnOutlined/>}
                                {index === 1 && <BlurOnOutlined/>}
                                {index === 2 && <BlurOnOutlined/>}
                            </ListItemIcon>
                            <ListItemText className={classes.listItemText}>
                                {additionalLinks.name}
                            </ListItemText>
                        </ListItem>
                    </Link>
                ))}
            </List>
        </div>
    );


    /* <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: '36px',
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            Mini variant drawer
          </Typography>
        </Toolbar>
    </AppBar> */


    /*  <Drawer
         anchor={anchor}
         open={state[anchor]}
         onClose={toggleDrawer(anchor, false)}
     >
        {list(anchor)}
     </Drawer> */


//      {isTabletOrMobile && handleDrawerClose}

    return (
        <div id='rootStyle' className={classes.root}>
            <CssBaseline/>
            {isTabletOrMobile &&
            <div style={{paddingTop: 0, position: 'fixed', width: 70, height: 50, background: '#c7d8c6', zIndex: 1}}>
                <AppBar
                    position="fixed"
                    className={clsx(classes.appBar, classes.paper, {
                        [classes.appBarShift]: open,
                    })}
                >
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            onClick={handleDrawerOpen}
                            edge="start"
                            className={clsx({
                                [classes.hide]: open,
                            })}
                        >
                            <MenuIcon/>
                        </IconButton>
                    </Toolbar>
                </AppBar>
            </div>}
            <Box
                role="presentation"
                onClick={handleDrawerClose}
                onKeyDown={handleDrawerClose}
                sx={{width: '100vw', display: 'flex'}}
            >
                {isDesktopOrLaptop && <Drawer
                    ref={targetRef}
                    id='rootSideDesk'
                    variant="permanent"
                    className={clsx(classes.drawer, {
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    })}
                    classes={{
                        paper: classes.paper,
                    }}
                >
                    {menuItems}
                </Drawer>}
                {isTabletOrMobile && <Drawer
                    ref={targetRef}
                    id='rootSideMobile'
                    variant='temporary'
                    open={open}

                    className={clsx(classes.drawer, {
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    })}
                    classes={{
                        paper: clsx({
                            [classes.drawerOpen]: open, [classes.paper]: open,
                            [classes.drawerClose]: !open,
                        }),
                    }}
                    ModalProps={{
                        keepMounted: true,
                    }}
                >
                    {menuItems}
                </Drawer>}
            </Box>
        </div>
    );
}

let addN01 = 73

export function MarginItemsLeft({value}: any) {
    addN01 = value;
    return <p>{addN01}</p>
}

export {addN01};

