import React, {ChangeEvent, FormEvent, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField,Card} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import StudentApi from "../api/StudentApi";
import {FormControl, InputLabel, Select, SelectChangeEvent} from "@mui/material";
import MenuItem from '@mui/material/MenuItem';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            //flexGrow: 1,
            minHeight: "38vh",
            width: "80%",
            margin: "20px",
            marginTop:"0px",
            background: "#Efd9c1",

        },
        form: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch',
            },
        },
        formdesign: {
            display: "flex",
            justifyContent: "around"
        },
        container:{
            textAlign:"right"
        },
        button:{
            background: "#efd9c1",
            margin: "20px"
        }


    }),
);
type FormData = {
    skill: string;
    level: string;
};
export default function StudentSkillForm() {
    const classes = useStyles();
    const {register, setValue, handleSubmit, formState: {errors}} = useForm<FormData>();
    const [skill, setSkill] = useState("");
    const [level, setLevel] = useState("");



    const handleSkillChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp= event.target.value;
        setSkill(temp);
    }
    const handleLevelChange = (event: SelectChangeEvent) => {
        let temp = event.target.value;
        setLevel(temp);
    }

    const onSubmit = handleSubmit((data) => {
        /* StudentApi.addStudent({
             firstName: data.firstName,
             lastName: data.lastName,
             password: data.password,
             email: data.email
         })
             .then(res => console.log(res))
             .catch(err => alert(err))*/
    });

    return (
        <>
            <Paper elevation={20}  className={classes.root}>
                <form className={classes.form} noValidate autoComplete="off" onSubmit={onSubmit}>
                    <TextField {...register("skill")}
                               size="small"
                               id="outlined-name-input"
                               label="Skill"
                               value={skill}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleSkillChange}
                    />
                    <FormControl style={{width: "46%"}}>
                        <InputLabel id="demo-simple-select-helper-label">Level</InputLabel>
                        <Select {...register("level")} style={{height:"40px",marginTop:"8px"}}
                            labelId="demo-simple-select-helper-label"
                            id="demo-simple-select-helper"
                            value={level}
                            label="Age"
                            onChange={handleLevelChange}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={"Begainner"}>Begainner</MenuItem>
                            <MenuItem value={"Moderate"}>Moderate</MenuItem>
                            <MenuItem value={"Expart"}>Expart</MenuItem>
                        </Select>
                    </FormControl>

                    <div className={classes.container} >
                        <Button type="submit" variant="outlined" className={classes.button}  >
                            ADD

                        </Button>
                    </div>

                </form>
            </Paper>
        </>
    )
}