import React, {ChangeEvent, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import StudentApi from "../api/StudentApi";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            backgroundColor:"#efd9c1"
        },
        form:{
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch',
            },}


    }),
);
type FormData = {
    file: string;
};

export default function StudentAddFileForm(){
    const classes = useStyles();
    const { register, setValue, handleSubmit, formState: { errors } } = useForm<FormData>();
    const [id,setId]=useState("");
    const [file,setFile]=React.useState<File>();
    const handleIdChange=(event: ChangeEvent<HTMLInputElement>)=>{
        let id = event.target.value;
        //ameValidation(newName);
        setId(id);
    }
    const handleFileChange=(event: ChangeEvent<HTMLInputElement>)=>{

        let fileList:any = event.target.files;
        if (!fileList)
            return null;
            setFile(fileList[0]);
        console.log(fileList[0]);
        }



    const onSubmit = handleSubmit((data) =>{
        const formData = new FormData();
      if(file){

          formData.append("file", file);
      }
      console.log(formData);

/*
        StudentApi.addStudentPic(pid,formData)
          .then(res=> console.log(res))
          .catch(err=> alert(err))*/
    } );
    return(
    <>
        <Paper elevation={20} className={classes.root} >
            <form className={classes.form} encType="multipart/form-data" onSubmit={onSubmit}>
                <TextField {...register("file")}
                            type="file"
                           id="outlined-lastname-input"
                           variant="outlined"
                           style={{width:"85%"}}
                    /*error={Boolean(nameErrors?.name)}
                    helperText={(nameErrors?.name)}*/
                           required
                           onChange={handleFileChange}

                />

                <Button type="submit"  color="primary"  >
                    Save

                </Button>


            </form>

        </Paper>
    </>

    )
}