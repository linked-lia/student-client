import React, {ChangeEvent, FormEvent, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField, Card, Typography} from "@material-ui/core";
import IconButton from "@mui/material/IconButton";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import DeleteIcon from '@mui/icons-material/Delete';
import EditRoundedIcon from '@mui/icons-material/EditRounded';
import DialogTitle from "@material-ui/core/DialogTitle";
const useStyles = makeStyles((theme) => ({
    root:{
        display:"flex",
        margin:"20px",
        padding: "10px",
        justifyContent: "space-between",
        backgroundColor:"#efd9c1"

    }
}));

export default function SimpleCard(props:any) {
    const classes = useStyles();
    const {company,position,from,to}=props;
    return (
        <Card elevation={20} className={classes.root}>
            <div>
                <Typography variant="h6" gutterBottom >
                    {company}
                </Typography>
                <Typography variant="subtitle1" gutterBottom >
                    {position}
                </Typography>
                <Typography variant="subtitle2" gutterBottom component="div">
                    {from}--{to}
                </Typography>
            </div>
            <div style={{textAlign:"center"}}>

                <IconButton   size="small" style={{backgroundColor:"#ddb6a2", marginRight:"10px",borderRadius:"50%",
                    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"}}>
                    <EditRoundedIcon style={{color:"#6f7fac"}}/>
                </IconButton>
                <IconButton  size="small" style={{backgroundColor:"#ddb6a2", marginRight:"5px",borderRadius:"50%",
                    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"}}>
                    <DeleteIcon style={{color:"#d70b44"}}/>
                </IconButton>
            </div>

        </Card>
    )
}