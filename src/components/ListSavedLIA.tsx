import * as React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Paper} from "@mui/material";
import SavedLIAListItemCard from "./SavedLIAListItemCard";

//TODO MAKE A NEW LIST!!!!!! **********

const useStyles = makeStyles({
    root: {
        maxHeight:"500px",
        width:"80%",
        overflow: "hidden",
        overflowY: "scroll",
        margin:"20px"
    },
    searchBar: {
        width:"80%",
        margin:"20px"
    }

});


export default function ListSavedLIA() {
    const classes = useStyles();

    return (

        <div>
            <Paper  elevation={3} className={classes.root}>
                <SavedLIAListItemCard/>
                <SavedLIAListItemCard/>
                <SavedLIAListItemCard/>
                <SavedLIAListItemCard/>
                <SavedLIAListItemCard/>
                <SavedLIAListItemCard/>
                <SavedLIAListItemCard/>
                <SavedLIAListItemCard/>
            </Paper>
        </div>
    );
}
