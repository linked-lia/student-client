import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    paper:{
        backgroundColor:"#efd9c1"
    }

}));

export default function PopupWindow(props:any) {
    const classes = useStyles();
    const {title, children, openWindow, closeWindow} = props;


    return (
        <div >
            <Dialog
                open={openWindow}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                PaperProps ={{
                    classes: {
                        root: classes.paper
                    }
                }}
            >
                <DialogTitle  style={{backgroundColor:"#ccc8c6"}} id="alert-dialog-title">Add {title}</DialogTitle>
                <DialogContent>
                    {children}
                </DialogContent>
                <DialogActions  style={{backgroundColor:"#ccc8c6"}}>
                    <Button onClick={closeWindow} color="primary">
                        Close
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
