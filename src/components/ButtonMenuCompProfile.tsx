import React from 'react';
import {makeStyles, createStyles, Theme} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {CardActionArea, Container, Grid, Paper} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import {ButtonUnstyled} from "@mui/material";
import {WallpaperRounded} from "@material-ui/icons";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            marginBottom: '1rem',
            [theme.breakpoints.up('sm')]: {
                marginBottom: '4rem',
            },
        },
        item: {
            marginLeft: 8,
            background: '#efd9c1',
            color: '#726e6e',
            textAlign: 'center',
            padding: 5
        },
    }),
);

export default function ButtonMenuCompProfile() {
    const classes = useStyles();

    return (
        <Container className={classes.root}>
            <Grid container spacing={1} justifyContent={'center'}>
                <Grid item md={2} sm={3} xs={12}>
                    <CardActionArea>
                        <Paper className={classes.item}>
                            Meddelande
                        </Paper>
                    </CardActionArea>
                </Grid>
                <Grid item md={2} sm={3} xs={12}>
                    <CardActionArea>
                        <Paper className={classes.item}>
                            Webbplats
                        </Paper>
                    </CardActionArea>
                </Grid>
                <Grid item md={2} sm={3} xs={12}>
                    <CardActionArea>
                        <Paper className={classes.item}>
                            Taggar
                        </Paper>
                    </CardActionArea>
                </Grid>
                <Grid item md={2} sm={3} xs={12}>
                    <Link to="/liaAdd">
                        <CardActionArea>
                            <Paper className={classes.item}>
                                LIA annonser
                            </Paper>
                        </CardActionArea>
                    </Link>
                </Grid>
            </Grid>
        </Container>
    );
}
