
import * as React from 'react';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import Container from "@material-ui/core/Container";
import Box from '@mui/material/Box';
import List from "@material-ui/core/List";
import Button from '@mui/material/Button';
import ListItem from '@mui/material/ListItem';
import FolderIcon from '@mui/icons-material/Folder';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import Avatar from '@mui/material/Avatar';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import {styled} from "@mui/material";







const subjectArea = [
    { label: 'JavaDeveloper', year: "2020" },
    { label: 'DataScience', year: "2021" },
    { label: 'FrontEndDeveloper', year: "2020" },
    { label: 'Testing', year: "2020" },
];


// For the list part

const Demo = styled('div')(({ theme }) => ({
   // backgroundColor: theme.palette.background.paper,
 }));

export default function FindCompany() {

    const [dense, setDense] = React.useState(false);
    const [secondary, setSecondary] = React.useState(false);

    return (

        <Container maxWidth="sm">
            <List>
            <Box sx={{ bgcolor: '#efd9c1', height: '100vh' }} />

        <TextareaAutosize  aria-label="empty textarea"
            placeholder="Company List"
            style={{ width: 400 }}
        />

                <Autocomplete
                    disablePortal
                    id="Company catagory by area/field"
                    options={subjectArea}
                    sx={{ width: 300 }}
                    renderInput={(params) => <TextField {...params} label="SubjectArea/field" />}
                />

                // search bar
                <Button variant="contained">search</Button>
            </List>

// Company list with image and .......
            <Box sx={{ flexGrow: 1, maxWidth: 752 }}>


                <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                        <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
                            Company list
                        </Typography>
                        <Demo>
                            <List dense={dense}>

                                    <ListItem
                                        secondaryAction={
                                            <IconButton edge="end" aria-label="delete">
                                                <DeleteIcon />
                                            </IconButton>
                                        }
                                    >
                                        <ListItemAvatar>
                                            <Avatar>
                                                <FolderIcon />
                                            </Avatar>
                                        </ListItemAvatar>
                                        <ListItemText
                                            primary="Spotify"
                                            secondary={secondary ? 'Secondary text' : null}
                                        />

                                    </ListItem>,

                            </List>
                        </Demo>
                    </Grid>
                </Grid>
            </Box>
            );
        </Container>


    );
}