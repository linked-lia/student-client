import React, {ChangeEvent, FormEvent, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField,Card} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import StudentApi from "../api/StudentApi";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            //flexGrow: 1,
            minHeight: "38vh",
            width: "80%",
            margin: "20px",
            marginTop:"0px",
            background: "#Efd9c1",

        },
        form: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch',
            },
        },
        formdesign: {
            display: "flex",
            justifyContent: "around"
        },
        container:{
            textAlign:"right"
        },
        button:{
            background: "#efd9c1",
            margin: "20px"
        }


    }),
);
type FormData = {
    school: string;
    degree: string;
    field: string;
    city: string;
    start: string;
    end: string;
};
export default function StudentEducationForm() {
    const classes = useStyles();
    const {register, setValue, handleSubmit, formState: {errors}} = useForm<FormData>();

    const [school, setSchool] = useState("");
    const [degree, setDegree] = useState("");
    const [field, setField] = useState("");
    const [city, setCity] = useState("");
    const [start, setStart] = useState("");
    const [end, setEnd] = useState("");



    const handleSchoolChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setSchool(temp);
    }
    const handleDegreeChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setDegree(temp);
    }
    const handleCityChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setCity(temp);
    }
    const handleFieldChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setField(temp);
    }
    const handleStartChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setStart(temp);
    }
    const handleEndChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setEnd(temp);
    }
    const onSubmit = handleSubmit((data) => {
        /* StudentApi.addStudent({
             firstName: data.firstName,
             lastName: data.lastName,
             password: data.password,
             email: data.email
         })
             .then(res => console.log(res))
             .catch(err => alert(err))*/
    });

    return (
        <>
            <Paper elevation={20}  className={classes.root}>
                <form className={classes.form} noValidate autoComplete="off" onSubmit={onSubmit}>
                    <TextField {...register("school")}
                               size="small"
                               id="outlined-name-input"
                               label="School"
                               value={school}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleSchoolChange}
                    />
                    <TextField {...register("degree")}
                               size="small"
                               id="outlined-lastname-input"
                               label="Degree"
                               value={degree}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleDegreeChange}
                    />

                    <TextField {...register("field")}
                               size="small"
                               id="outlined-lastname-input"
                               label="Field"
                               value={field}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleFieldChange}
                    />
                    <TextField {...register("city")}
                               size="small"
                               id="outlined-lastname-input"
                               label="City"
                               value={city}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleCityChange}
                    />
                    <TextField {...register("start")}
                               size="small"
                               id="outlined-lastname-input"
                               label="From"
                               value={start}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleStartChange}
                    />
                    <TextField {...register("end")}
                               size="small"
                               id="outlined-lastname-input"
                               label="End"
                               value={start}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleEndChange}
                    />
                    <div className={classes.container} >
                        <Button type="submit" variant="outlined" className={classes.button}  >
                            ADD

                        </Button>
                    </div>

                </form>
            </Paper>
        </>
    )
}