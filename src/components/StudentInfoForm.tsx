import React, {ChangeEvent, FormEvent, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import { Paper, TextField,Card} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import StudentApi from "../api/StudentApi";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            //flexGrow: 1,
            maxHeight:"80vh",
            width: "60%",
            margin:"20px",
            marginTop:"0px",
            background:"#efd9c1",

        },
        form:{
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch',
            },},
        formdesign:{
            display:"flex",
            justifyContent:"around"
        }


    }),
);
type FormData = {
    firstName: string;
    lastName: string;
    password:string;
    confirmpassword:string;
    email:string;
    phone:string;
    address:string;
    postcode:string;
    area:string;
    linkedin:string;
    gitlab:string;
    bio:string;

};
export default function StudentInfoForm(){
    const classes = useStyles();
    const { register, setValue, handleSubmit, formState: { errors } } = useForm<FormData>();

    const [firstName,setFirstName]=useState("");
    const [lastName,setLastName]=useState("");
    const [password,setPassword]=useState("");
    const [email,setEmail]=useState("");
    const [confirmpassword,setConfirmpassword]=useState("");
    const [phone,setPhone]=useState("");
    const [address,setAddress]=useState("");
    const [postcode,setPostcode]=useState("");
    const [area,setArea]=useState("");
    const [linkedin,setLinkedin]=useState("");
    const [gitlab,setGitlab]=useState("");
    const [bio,setBio]=useState("");


    const handleFirstNameChange=(event: ChangeEvent<HTMLInputElement>)=>{
        let newName = event.target.value;
        //ameValidation(newName);
        setFirstName(newName);
    }
    const handleLastNameChange=(event: ChangeEvent<HTMLInputElement>)=>{
        let newName = event.target.value;
        //ameValidation(newName);
        console.log(newName)
        setLastName(newName);
    }
    const handlePasswordChange=(event: ChangeEvent<HTMLInputElement>)=>{
        let pass = event.target.value;
        //ameValidation(newName);
        setPassword(pass);
    }
    const handleEmailChange=(event: ChangeEvent<HTMLInputElement>)=>{
        let newemail = event.target.value;
        //ameValidation(newName);
        console.log(newemail)
        setEmail(newemail);

    }
    const onSubmit = handleSubmit((data) =>{
        StudentApi.addStudent({firstName:data.firstName,lastName:data.lastName, password:data.password,email:data.email})
            .then(res=> console.log(res))
            .catch(err=> alert(err))
    } );
    /*console.log(firstName);
    const handleSubmit=(e:FormEvent<HTMLInputElement>)=>{
        //e.preventDefault();
console.log("this is",firstName);
        /!*setFirstName("");
        setLastName("");
        setPassword("");
        setEmail("");*!/
        console.log(firstName);

    }*/


    return(
        <>
            <Paper elevation={20} className={classes.root} >
                <form className={classes.form} noValidate autoComplete="off"  onSubmit={onSubmit}>
                    <div className={classes.formdesign}>
                    <TextField {...register("firstName")}
                               size="small"
                               id="outlined-name-input"
                               label="FirstName"
                               value={firstName}
                               variant="outlined"
                               style={{width:"46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleFirstNameChange}

                    />
                    <TextField {...register("lastName")}
                               size="small"
                               id="outlined-lastname-input"
                               label="LastName"
                               value={lastName}
                               variant="outlined"
                               style={{width:"46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleLastNameChange}

                    />
                    </div>
                    <div className={classes.formdesign}>
                    <TextField {...register("password")}
                               size="small"
                               id="outlined-password-input"
                               label="Password"
                               value={password}
                               variant="outlined"
                               style={{width:"46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handlePasswordChange}

                    />

                    <TextField {...register("confirmpassword")}
                               size="small"
                               id="outlined-password-input"
                               label="Confirm Password"
                               value={confirmpassword}
                               variant="outlined"
                               style={{width:"46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handlePasswordChange}

                    />
                    </div>
                    <div className={classes.formdesign}>
                    <TextField {...register("email")}
                               size="small"
                               id="outlined-email-input"
                               label="E-mail"
                               value={email}
                               variant="outlined"
                               style={{width:"46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleEmailChange}

                    />
                    <TextField {...register("phone")}
                               size="small"
                               id="outlined-email-input"
                               label="Phone"
                               value={phone}
                               variant="outlined"
                               style={{width:"46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handlePasswordChange}
                    />
                    </div>
                    <div className={classes.formdesign}>
                        <TextField {...register("address")}
                                   size="small"
                                   id="outlined-email-input"
                                   label="Address"
                                   value={address}
                                   variant="outlined"
                                   style={{width:"41%"}}
                            /*error={Boolean(nameErrors?.name)}
                            helperText={(nameErrors?.name)}*/
                                   required
                                   onChange={handlePasswordChange}
                        />
                        <TextField {...register("postcode")}
                                   size="small"
                                   id="outlined-email-input"
                                   label="Postcode"
                                   value={postcode}
                                   variant="outlined"
                                   style={{width:"25%"}}
                            /*error={Boolean(nameErrors?.name)}
                            helperText={(nameErrors?.name)}*/
                                   required
                                   onChange={handlePasswordChange}
                        />
                        <TextField {...register("area")}
                                   size="small"
                                   id="outlined-email-input"
                                   label="Area"
                                   value={area}
                                   variant="outlined"
                                   style={{width:"25%"}}
                            /*error={Boolean(nameErrors?.name)}
                            helperText={(nameErrors?.name)}*/
                                   required
                                   onChange={handlePasswordChange}
                        />
                    </div>
                    <div className={classes.formdesign}>
                        <TextField {...register("linkedin")}
                                   size="small"
                                   id="outlined-email-input"
                                   label="Linkedin"
                                   value={linkedin}
                                   variant="outlined"
                                   style={{width:"46%"}}
                            /*error={Boolean(nameErrors?.name)}
                            helperText={(nameErrors?.name)}*/
                                   required
                                   onChange={handlePasswordChange}
                        />
                        <TextField {...register("gitlab")}
                                   size="small"
                                   id="outlined-email-input"
                                   label="Gitlab"
                                   value={gitlab}
                                   variant="outlined"
                                   style={{width:"46%"}}
                            /*error={Boolean(nameErrors?.name)}
                            helperText={(nameErrors?.name)}*/
                                   required
                                   onChange={handlePasswordChange}
                        />

                    </div>
                    <TextField {...register("bio")}
                               size="small"
                               id="outlined-email-input"
                               label="BIO"
                               value={bio}
                               variant="outlined"
                               style={{width:"98%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handlePasswordChange}
                    />

                    <Button type="submit"    >
                        Save

                    </Button>


                </form>

            </Paper>
        </>
    )
}