import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import OrionLondonlogo from '../assets/media/OrionLondonlogo.png';
import {Link} from 'react-router-dom';


const useStyles = makeStyles({
    root: {
        maxWidth: "100%",
        background: "#efd9c1",
        flex: "row",
        display: "flex",
        alignItems: "left"
    },
    titleText: {
        color: "#726e6e",
        fontWeight: "bold"
    },
    companyName: {
        color: "#726e6e",
        fontWeight: "bold"
    },
    city: {
        color: '#726e6e',
        marginTop: 10
    },
    imgSize: {
        marginTop: 30,
        height: 40,
        width: 70
    },
    saveButton: {
        color: '#726e6e'
    },
    activatedSaveButton: {
        color: '#726e6e',
        background: '#ad9b8f'
    }
});

export default function ListItemCard() {
    const classes = useStyles();

    const [save, setSave] = useState(false);

    return (
        <Card className={classes.root}>
            <Link to="/companyProfile">
                <CardMedia className={classes.imgSize}
                           component="img"
                           alt="Company logo"
                    /*                        height="30"
                                            width="30"*/
                           image={OrionLondonlogo}
                           title="Company logo"
                />
            </Link>
            <CardActionArea>
                <Link to="/liaAdd">
                    <CardContent>
                        <Typography className={classes.titleText} gutterBottom variant="h5" component="h2">
                            Jobbtitel
                        </Typography>
                        <Typography variant="body2" className={classes.companyName}>
                            TEAM LIA
                        </Typography>
                        <Typography variant="body2" className={classes.city}>
                            Stockholm
                        </Typography>
                    </CardContent>
                </Link>
            </CardActionArea>
            <CardActions>
                {!save ?
                    <Button onClick={() => setSave(true)} className={classes.saveButton} size="small"
                            variant="outlined">
                        SPARA
                    </Button>
                    :
                    <Button onClick={() => setSave(false)} className={classes.activatedSaveButton} size="small"
                            variant="outlined">
                        SPARAD
                    </Button>
                }
            </CardActions>
        </Card>
    );
}
