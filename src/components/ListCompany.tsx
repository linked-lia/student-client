import * as React from 'react';
import ListItemCardCompany from "./ListItemCardCompany";
import {makeStyles} from "@material-ui/core/styles";
import {Paper} from "@mui/material";


//TODO MAKE A NEW LIST!!!!!! **********

const useStyles = makeStyles({
    root: {
        maxHeight: "500px",
        width: "80%",
        overflow: "hidden",
        overflowY: "scroll",
        margin: "20px"
    },
    searchBar: {
        width: "80%",
        margin: "20px"
    }

});


const ListCompany = () => {

    const companyList = [
        {name: "Spotify", place: "Stockholm", image: "Spotify"},
        {name: "Scania", place: "Södertälje", image: "scania"},
        {name: "IKEA", place: "malmo", image: "Ikea"},
        {name: "EC", place: "Solna", image: "Ec"},
        {name: "Google", place: "London", image: "Google"},
    ];


    const classes = useStyles();

    return (

        <div>
            <Paper elevation={3} className={classes.root}>
                <section>

                    {companyList.map((company, index) =>
                        <ListItemCardCompany key={index} images={company.image}
                                             name={company.name}
                                             place={company.place}/>
                    )}


                </section>
            </Paper>
        </div>


    );
};

export default ListCompany;
