import * as React from 'react';
import ListItemCard from "./ListItemCard";
import {makeStyles} from "@material-ui/core/styles";
import {Paper} from "@mui/material";
import SearchBar from "material-ui-search-bar";

//TODO MAKE A NEW LIST!!!!!! **********

const useStyles = makeStyles({
    root: {
        maxHeight:"500px",
        width:"80%",
        overflow: "hidden",
        overflowY: "scroll",
        margin:"20px"
    },
    searchBar: {
        width:"80%",
        margin:"20px"
    }

});


export default function ListLIA() {
    const classes = useStyles();

    return (

        <div>
            <Paper  elevation={3} className={classes.root}>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
            </Paper>
        </div>


    );
}
