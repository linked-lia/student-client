import React, {ChangeEvent, FormEvent, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField,Card} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import StudentApi from "../api/StudentApi";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            //flexGrow: 1,
            minHeight: "38vh",
            width: "80%",
            margin: "20px",
            marginTop:"0px",
            background: "#Efd9c1",

        },
        form: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch',
            },
        },
        formdesign: {
            display: "flex",
            justifyContent: "around"
        },
        container:{
            textAlign:"right"
        },
        button:{
            background: "#efd9c1",
            margin: "20px"
        }


    }),
);
type FormData = {
    company: string;
    position: string;
    city: string;
    description: string;
    start: string;
    end: string;
};
export default function StudentWorkForm() {
    const classes = useStyles();
    const {register, setValue, handleSubmit, formState: {errors}} = useForm<FormData>();

    const [company, setCompany] = useState("");
    const [position, setPosition] = useState("");
    const [password, setPassword] = useState("");
    const [city, setCity] = useState("");
    const [description, setDescription] = useState("");
    const [start, setStart] = useState("");
    const [end, setEnd] = useState("");



    const handleCompanyChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setCompany(temp);
    }
    const handlePositionChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setPosition(temp);
    }
    const handleCityChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setCity(temp);
    }
    const handleDescriptionChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setDescription(temp);
    }
    const handleStartChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setStart(temp);
    }
    const handleEndChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setEnd(temp);
    }
   const onSubmit = handleSubmit((data) => {
       /* StudentApi.addStudent({
            firstName: data.firstName,
            lastName: data.lastName,
            password: data.password,
            email: data.email
        })
            .then(res => console.log(res))
            .catch(err => alert(err))*/
    });

    return (
        <>
            <Paper elevation={20}  className={classes.root}>
                <form className={classes.form} noValidate autoComplete="off" onSubmit={onSubmit}>
                        <TextField {...register("company")}
                                   size="small"
                                   id="outlined-name-input"
                                   label="Company"
                                   value={company}
                                   variant="outlined"
                                   style={{width: "46%"}}
                            /*error={Boolean(nameErrors?.name)}
                            helperText={(nameErrors?.name)}*/
                                   required
                                   onChange={handleCompanyChange}
                        />
                        <TextField {...register("position")}
                                   size="small"
                                   id="outlined-lastname-input"
                                   label="Position"
                                   value={position}
                                   variant="outlined"
                                   style={{width: "46%"}}
                            /*error={Boolean(nameErrors?.name)}
                            helperText={(nameErrors?.name)}*/
                                   required
                                   onChange={handlePositionChange}
                        />
                    <TextField {...register("city")}
                               size="small"
                               id="outlined-lastname-input"
                               label="City"
                               value={city}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handlePositionChange}
                    />
                    <TextField {...register("description")}
                               size="small"
                               id="outlined-lastname-input"
                               label="description"
                               value={description}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleDescriptionChange}
                    />
                    <TextField {...register("start")}
                               size="small"
                               id="outlined-lastname-input"
                               label="From"
                               value={start}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleStartChange}
                    />
                    <TextField {...register("end")}
                               size="small"
                               id="outlined-lastname-input"
                               label="End"
                               value={start}
                               variant="outlined"
                               style={{width: "46%"}}
                        /*error={Boolean(nameErrors?.name)}
                        helperText={(nameErrors?.name)}*/
                               required
                               onChange={handleEndChange}
                    />
                    <div className={classes.container} >
                        <Button type="submit" variant="outlined" className={classes.button}  >
                            ADD

                        </Button>
                    </div>

                </form>
            </Paper>
        </>
    )
}