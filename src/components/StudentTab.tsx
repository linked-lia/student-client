import React from "react";
import {Grid} from "@material-ui/core";
import { makeStyles} from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import {Link} from "react-router-dom";


function StudentTab() {

    const useStyles = makeStyles({

        main: {
            backgroundColor: '#ccc8c6',
            minHeight: '5vh',
            width: '100',
            paddingTop: '1px',

        },
        image: {
            height: '80px',
            width: '100px',
            paddingLeft: '30px',

        },
        nav: {
            display: 'flex',
        },
        listItem: {
            margin: '1px',
            textAlign: 'center',
            borderRadius: '.5',
            '&:hover': {
                backgroundColor: '#efd9c1'
            },

        },
        listText: {
            fontWeight: 'bold',
            fontSize: '.5rem',
            color: 'white',
            textDecoration: 'none',
        }
    });
    const classes = useStyles();
    return (
// this is for main css
        <div className={classes.main}>
            <Grid container>
                <Grid item xs={12} md={7} style={{padding: '1px'}}>
                    <List component="nav"  className={classes.nav}>
                        <ListItem className={classes.listItem}>
                            <Link to="/" className={classes.listText}>
                                HOME
                            </Link>

                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <Link to="/User" className={classes.listText}>
                                USER
                            </Link>
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <Link to="/right" className={classes.listText}>
                                RIGHT
                            </Link>
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <Link to="/domain" className={classes.listText}>
                                DOMAIN
                            </Link>
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <Link to="/RightUser" className={classes.listText}>
                                USER RIGHTS
                            </Link>
                        </ListItem>

                    </List>
                </Grid>

            </Grid>

        </div>
    );
}

export default StudentTab;
