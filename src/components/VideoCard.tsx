import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import giphy from '../assets/media/giphy.gif';
import {CardContent, Collapse, Grid, IconButton, IconButtonProps, styled, Typography} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlayCircle} from "@fortawesome/free-solid-svg-icons";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";

//TODO Delete this, cuz we will probably not use.

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        playButton: {
            display: 'grid',
            justifyContent: 'center',
            zIndex: 1
        },
        position: {
            justifyContent: 'center',
            [theme.breakpoints.up('sm')]: {
                justifyContent: 'flex-start',
            }
        },
        cardContent: {
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block'
            }
        }
    })
);

export default function VideoCard() {
    const classes = useStyles();

    return (
        <Grid container className={classes.position} >
            <Grid item >
                <Card sx={{maxWidth: 345}}>
                        <CardMedia
                            component="img"
                            alt='monkey'
                            image={giphy}
                            // autoPlay
                        />
                    <Grid item className={classes.playButton}>
                        <IconButton  onClick={() => {alert('Länka till Youtube Video!');}}>
                            <FontAwesomeIcon icon={faPlayCircle}/>
                        </IconButton>
                    </Grid>
                    <CardContent >
                        <Typography gutterBottom variant="h5" component="div">
                            Prektisera hos oss!
                        </Typography>
                        <Typography variant="body2" className={classes.cardContent}>
                           Välkommna att ansöka LIA hos drömföretaget!
                            Vi är bäst på alla sätt. Finns inte så mycket mer att säga.
                            Vi ses snart! :)
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    )
        ;
}
